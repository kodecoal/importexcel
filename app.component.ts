import { Component } from '@angular/core';
import * as Excel from '@grapecity/spread-excelio';
import { Spread } from '@grapecity/spread-sheets';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  hostStyle = {
    width: '95vw',
    height: '80vh'
  };
  private spread: Spread.Sheets.Workbook;
  private excelIO;
  constructor() {
    this.excelIO = new Excel.IO();
  }
  workbookInit(args) {
    const self = this;
    self.spread = args.spread;
  }
  onFileChange(args) {
    const self = this, file = args.srcElement && args.srcElement.files && args.srcElement.files[0];
    if (self.spread && file) {
      self.excelIO.open(file, (json) => {
        self.spread.fromJSON(json, {});
        setTimeout(() => {
          alert('load successfully');
        }, 0);
      }, (error) => {
        alert('load fail');
      });
    }
  }
}
